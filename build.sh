#! /bin/bash

# Build web UI
cd ~/go/src/gitee.com/shanghaitech-university-codec_0/video_server
go install
mkdir  ~/go/bin/video_server_web_ui
cp ~/go/bin/web ~/go/bin/video_server_web_ui/web
cp -R ~/go/src/gitee.com/shanghaitech-university-codec_0/video_server/templates ~/go/bin/video_server_web_ui/
