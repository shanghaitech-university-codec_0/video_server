package main

import (
	"context"
	"log"
	"net/http"
	"net/url"

	"gitee.com/shanghaitech-university-codec_0/video_server/config"
	"github.com/tencentyun/cos-go-sdk-v5"
)

func UploadToOss(filename string, path string) bool {
	var err error
	u, _ := url.Parse("https://" + config.GetOssAddr())
	// 用于Get Service 查询，默认全地域 service.cos.myqcloud.com
	su, _ := url.Parse("https://cos.ap-shanghai.myqcloud.com")
	b := &cos.BaseURL{BucketURL: u, ServiceURL: su}
	// 1.永久密钥
	client := cos.NewClient(b, &http.Client{
		Transport: &cos.AuthorizationTransport{
			SecretID:  "AKIDgDVYLsACyUkma9TfXBbmiOp1yg9ML5Zp",
			SecretKey: "3rQkpgIfoiNnyua1Ci16vUmoa6MM6aom",
		},
	})

	if client == nil {
		log.Printf("Init oss service error")
		return false
	}
	_, err = client.Object.PutFromFile(context.Background(), filename, path, nil)
	if err != nil {
		log.Printf("Uploading object error: %s", err)
		return false
	}

	return true
}
