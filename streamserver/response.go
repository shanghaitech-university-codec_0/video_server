/*
 * @Descripttion:
 * @version:
 * @Author: congsir
 * @Date: 2022-01-09 15:45:07
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-01-09 15:47:02
 */
package main

import (
	"io"
	"net/http"
)

func sendErrorResponse(w http.ResponseWriter, sc int, errMsg string) {
	w.WriteHeader(sc)
	io.WriteString(w, errMsg)
}
