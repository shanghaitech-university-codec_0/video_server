/*
 * @Descripttion:
 * @version:
 * @Author: congsir
 * @Date: 2022-01-09 11:40:03
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-01-18 18:00:01
 */

package main

import (
	"net/http"

	"gitee.com/shanghaitech-university-codec_0/video_server/api/defs"
	"gitee.com/shanghaitech-university-codec_0/video_server/api/session"
)

var HEADER_FIELD_SESSION = "X-Session-Id"
var HEADER_FIELD_UNAME = "X-User-Name"

// Check if the current user has the permission
// Use session id to do the check
func validateUserSession(r *http.Request) bool {
	sid := r.Header.Get(HEADER_FIELD_SESSION)
	if len(sid) == 0 {
		return false
	}

	uname, ok := session.IsSessionExpired(sid)
	if ok {
		return false
	}

	r.Header.Add(HEADER_FIELD_UNAME, uname)
	return true
}

func ValidateUser(w http.ResponseWriter, r *http.Request) bool {
	uname := r.Header.Get(HEADER_FIELD_UNAME)
	if len(uname) == 0 {
		sendErrorResponse(w, defs.ErrorNotAuthUser)
		return false
	}

	return true
}
