/*
 * @Descripttion:
 * @version:
 * @Author: congsir
 * @Date: 2022-01-04 22:29:52
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-01-18 18:01:00
 */
package main

import (
	"encoding/json"
	"io"
	"net/http"

	"gitee.com/shanghaitech-university-codec_0/video_server/api/defs"
)

func sendErrorResponse(w http.ResponseWriter, errResp defs.ErrResponse) {
	w.WriteHeader(errResp.HttpSC)

	resStr, _ := json.Marshal(&errResp.Error)
	io.WriteString(w, string(resStr))
}

func sendNormalResponse(w http.ResponseWriter, resp string, sc int) {
	w.WriteHeader(sc)
	io.WriteString(w, resp)
}
