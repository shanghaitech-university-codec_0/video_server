/*
 * @Descripttion:
 * @version:
 * @Author: congsir
 * @Date: 2022-01-18 16:54:12
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-01-18 22:21:22
 */
package main

type ApiBody struct {
	Url     string `json:"url"`
	Method  string `json:"method"`
	ReqBody string `json:"req_body"`
}

type Err struct {
	Error     string `json:"error"`
	ErrorCode string `json:"error_code"`
}

var (
	ErrorRequestNotRecognized   = Err{Error: "api not recognized, bad resquest", ErrorCode: "001"}
	ErrorRequestBodyParseFailed = Err{Error: "request body is not correct", ErrorCode: "002"}
	ErrorInternalFaults         = Err{Error: "internal service error", ErrorCode: "003"}
)
