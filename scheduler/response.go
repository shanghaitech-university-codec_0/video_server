/*
 * @Descripttion:
 * @version:
 * @Author: congsir
 * @Date: 2022-01-14 14:57:14
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-01-18 12:03:45
 */
package main

import (
	"io"
	"net/http"
)

func sendResponse(w http.ResponseWriter, sc int, resp string) {
	w.WriteHeader(sc)
	io.WriteString(w, resp)
}
