/*
 * @Descripttion:
 * @version:
 * @Author: congsir
 * @Date: 2022-01-14 14:59:31
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-01-18 10:27:03
 */
package taskrunner

import (
	"time"
)

type Worker struct {
	ticker *time.Ticker
	runner *Runner
}

func NewWorker(interval time.Duration, r *Runner) *Worker {
	return &Worker{
		ticker: time.NewTicker((interval * time.Second)),
		runner: r,
	}
}

func (w *Worker) startWorker() {
	for {
		select {
		case <-w.ticker.C:
			go w.runner.StartAll()
		}
	}
}

func Start() {
	r := NewRunner(3, true, VideoClearDispatcher, VideoClearExecutor)
	w := NewWorker(3, r)
	go w.startWorker()
}
