/*
 * @Descripttion:
 * @version:
 * @Author: congsir
 * @Date: 2022-01-14 14:59:21
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-01-18 17:57:41
 */
package taskrunner

import (
	"errors"
	"log"
	"net/http"
	"sync"

	"paul/config"

	"gitee.com/shanghaitech-university-codec_0/video_server/scheduler/dbops"
	"gitee.com/shanghaitech-university-codec_0/video_server/scheduler/ossops"
)

func deleteVideo(vid string) error {
	ossfn := "videos/" + vid
	ok := ossops.DeleteObject(ossfn)

	if !ok {
		log.Printf("Deleting video error, oss operation failed")
		return errors.New("Deleting video error")
	}

	return nil
}

func VideoClearDispatcher(dc dataChan) error {
	res, err := dbops.ReadVideoDeletionRecord(3)
	if err != nil {
		log.Printf("Video clear dispatcher error: %v", err)
		return err
	}

	if len(res) == 0 {
		return errors.New("All tasks finished")
	}

	for _, id := range res {
		dc <- id
	}
	return nil
}

func VideoClearExecutor(dc dataChan) error {
	errMap := &sync.Map{}
	var err error

forloop:
	for {
		select {
		case vid := <-dc:
			go func(id interface{}) {
				if err := deleteVideo(id.(string)); err != nil {
					errMap.Store(id, err)
					return
				}
				if err := dbops.DelVideoDeletionRecord(id.(string)); err != nil {
					errMap.Store(id, err)
					return
				}
			}(vid)
		default:
			break forloop
		}
	}
	errMap.Range(func(k, v interface{}) bool {
		err = v.(error)
		if err != nil {
			return false
		}
		return true
	})

	return err
}

func SendDeleteVideoRequest(id string) {
	addr := config.GetLbAddr() + ":9001"
	url := "http://" + addr + "/video-delete-record/" + id
	_, err := http.Get(url)
	if err != nil {
		log.Printf("Sending deleting video request error: %s", err)
	}
}
