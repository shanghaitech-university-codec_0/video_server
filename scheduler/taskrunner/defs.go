/*
 * @Descripttion:
 * @version:
 * @Author: congsir
 * @Date: 2022-01-14 14:59:52
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-01-18 12:22:12
 */
package taskrunner

const (
	READY_TO_DISPATCH = "d"
	READY_TO_EXECUTE  = "e"
	CLOSE             = "c"
	VIDEO_PATH        = "../streamserver/videos/"
)

type controlChan chan string

type dataChan chan interface{}

type fn func(dc dataChan) error
