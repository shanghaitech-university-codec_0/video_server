/*
 * @Descripttion:
 * @version:
 * @Author: congsir
 * @Date: 2022-01-17 09:38:05
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-01-19 15:25:18
 */
/*
 * @Descripttion:
 * @version:
 * @Author: congsir
 * @Date: 2022-01-04 09:56:00
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-01-04 21:53:41
 */
package dbops

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

var (
	dbConn *sql.DB
	err    error
)

func init() {
	dbConn, err = sql.Open("mysql", "root:123456@(127.0.0.1:3306)/video_server")
	if err != nil {
		panic(err.Error())
	}
}
