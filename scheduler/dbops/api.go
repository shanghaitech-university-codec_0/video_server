/*
 * @Descripttion:
 * @version:
 * @Author: congsir
 * @Date: 2022-01-17 09:37:54
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-01-18 11:44:03
 */
package dbops

import (
	"log"

	_ "github.com/go-sql-driver/mysql"
)

func AddVideoDeletionRecord(vid string) error {
	stmtIns, err := dbConn.Prepare("INSERT INTO video_del_rec (video_id) VALUES(?)")
	if err != nil {
		return err
	}

	_, err = stmtIns.Exec(vid)
	if err != nil {
		log.Printf("AddVideoDeletionRecord error: %v", err)
		return err
	}

	defer stmtIns.Close()
	return nil
}
