/*
 * @Descripttion:
 * @version:
 * @Author: congsir
 * @Date: 2022-01-14 14:57:05
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-01-18 11:48:51
 */
package main

import (
	"net/http"

	"gitee.com/shanghaitech-university-codec_0/video_server/scheduler/taskrunner"
	"github.com/julienschmidt/httprouter"
)

func RegisterHandlers() *httprouter.Router {
	router := httprouter.New()

	router.GET("/video-delete-record/:vid-id", vidDelRecHandler)

	return router
}

func main() {
	go taskrunner.Start()
	r := RegisterHandlers()
	http.ListenAndServe(":9001", r)
}
