/*
 * @Descripttion:
 * @version:
 * @Author: congsir
 * @Date: 2022-01-18 22:11:10
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-01-19 15:41:31
 */
package config

import (
	"encoding/json"
	"log"
	"os"
)

type Configuration struct {
	LBAddr  string `json:"lb_addr"`
	OssAddr string `json:"oss_addr"`
}

var configuration *Configuration

func init() {
	file, _ := os.Open("./conf.json")
	defer file.Close()
	decoder := json.NewDecoder(file)
	configuration = &Configuration{}

	err := decoder.Decode(configuration)
	if err != nil {
		log.Printf("Decode json error: %s", err)
	}
}

func GetLbAddr() string {
	return configuration.LBAddr
}

func GetOssAddr() string {
	return configuration.OssAddr
}
