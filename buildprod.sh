#! /bin/bash
###
 # @Descripttion: 
 # @version: 
 # @Author: congsir
 # @Date: 2022-01-19 14:15:28
 # @LastEditors: Please set LastEditors
 # @LastEditTime: 2022-01-19 16:47:52
### 

# Build web and other services

cd ~/Applications/Go/src/gitee.com/shanghaitech-university-codec_0/video_server/api
env GOOS=linux GOARCH=amd64 go build -o ../bin/api

cd ~/Applications/Go/src/gitee.com/shanghaitech-university-codec_0/video_server/scheduler
env GOOS=linux GOARCH=amd64 go build -o ../bin/scheduler

cd ~/Applications/Go/src/gitee.com/shanghaitech-university-codec_0/video_server/streamserver
env GOOS=linux GOARCH=amd64 go build -o ../bin/streamserver

cd ~/Applications/Go/src/gitee.com/shanghaitech-university-codec_0/video_server/web
env GOOS=linux GOARCH=amd64 go build -o ../bin/web